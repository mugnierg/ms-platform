# MindSpore Platform for non-expert in AI
This project is based on Django and aims to develop a non-expert GUI that pilot a MindSpore project of image segmentation.

## Requirements
Use pipenv to install the dependencies needed :
```bash
pipenv shell
pipenv install Pipfile.lock
```

