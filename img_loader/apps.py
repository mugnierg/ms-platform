from django.apps import AppConfig


class ImgLoaderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'img_loader'
